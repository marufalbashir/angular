import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import { Student } from '../student';
import { StudentService } from './student';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public table_class="table_border";
  public btn_info="btn_info";
  public btn_danger="btn_danger";
  public search_input ="search_input";
 
  private _url:string="https://jsonplaceholder.typicode.com/users";
  constructor(private http:HttpClient) { }
  public student =[];
  constructor(private _studentService: StudentService) { }
  ngOnInit() {
    
    this._studentService.Student()
    .subscribe(data=>this.students=data);
  
  };
  getStudents():Observable<Student[]>{
    return this.http.get<Student>(this._url);
    
  } 
}
